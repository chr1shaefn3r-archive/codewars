fn no_space(x : String) -> String{
    x.chars().filter(|x| x != &' ').collect()
}
