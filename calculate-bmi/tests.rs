#[test]
fn default_unit_tests() {
  assert_eq!(bmi(50, 1.80), "Underweight");
  assert_eq!(bmi(80, 1.80), "Normal");
  assert_eq!(bmi(90, 1.80), "Overweight");
  assert_eq!(bmi(110, 1.80), "Obese");
}
