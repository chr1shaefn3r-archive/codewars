fn bmi(weight: u32, height: f32) -> &'static str {
  let weight: f32 = weight as f32;
  let bmi = weight / (height.powi(2));
  if bmi <= 18.5 {
    return "Underweight"
  } else if bmi <= 25.0 {
    return "Normal"
  } else if bmi <= 30.0 {
    return "Overweight"
  }
  return "Obese";
}
