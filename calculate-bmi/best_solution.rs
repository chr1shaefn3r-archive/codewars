fn bmi(weight: u32, height: f32) -> &'static str {
  return match weight as f32 / height.powi(2) {
    _ if bmi <= 18.5 => "Underweight",
    _ if bmi <= 25.0 => "Normal",
    _ if bmi <= 30.0 => "Overweight",
    _ => "Obese"
  }
}
