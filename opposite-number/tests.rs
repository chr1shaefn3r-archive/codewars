#[test]
fn returns_expected() {
  assert_eq!(opposite(1), -1);
  assert_eq!(opposite(-1), 1);
}
