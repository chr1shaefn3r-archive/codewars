fn evaporator(content: f64, evap_per_day: i32, threshold: i32) -> i32 {
    let threshold_in_ml: f64 = content * (threshold as f64 / 100.);
    let mut content_left: f64 = content;
    let mut days: i32 = 1;
    loop {
        let ml_left = content_left * (evap_per_day as f64 / 100.);
        content_left -= ml_left;
        if content_left < threshold_in_ml {
            break;
        } else {
            days += 1;
        }
    }
    days
}
