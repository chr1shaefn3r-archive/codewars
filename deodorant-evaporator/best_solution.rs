fn evaporator(_content: f64, evap_per_day: i32, threshold: i32) -> i32 {
    return (((threshold as f64) / 100.0).ln() / (1.0 - (evap_per_day as f64) / 100.0).ln()).ceil() as i32;
}
