# codewars

>> My codewars.com katas

I'm trying to up my rust game by doing one kata a day.

## folder structure

Foldername is the url name of the kata, so that it can be looked up with:
https://www.codewars.com/kata/<folder_name>
The files are always the same:
 * my\_solution.rs: The solution I submitted (this could get embarasing tbh)
 * best\_solution.rs: What I consider the best solution (mostly a mix of the highest voted solutions on codewars)
 * test.rs: The unit tests the solutions were verified against with

## Add kata

```bash
mkdir -p ./<KATA_NAME>
vim {best_solution,my_solution,tests}.rs
```

## TODO
Get to a setup where my\_solution and best\_solution can easily be verified against test.rs
